﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour

{
    public CharacterController2D controller;
    public Animator animator;
    public GameObject deathEffect;

    public float runSpeed = 40f;

    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;
    bool shoot = false;

 

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal")* runSpeed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if(Input.GetButtonDown("Jump"))
        {
            jump = true;
            animator.SetBool("isJumping", true); 
        }
        if (Input.GetButtonDown("Crouch"))
        {
            crouch  = true;
        }
        else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false; 
        }
        if (Input.GetButtonDown("Fire1"))
        {
            shoot = true;
            animator.SetBool("isShooting", true);
        }
        else 
        {
            shoot = false;
            animator.SetBool("isShooting", false);
        }

    }



    public void OnLanding()
    {
        animator.SetBool("isJumping", false);
    }

    public void OnCrouching(bool isCrouching)
    {
        animator.SetBool("isCrouching", isCrouching);
    }





     


    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
        

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        
        if(other.gameObject.tag =="damage")
        {
            Die();
        }
        if (other.gameObject.tag == "END")
        {
            SceneManager.LoadScene("GameOver");
        }
    }
    void Die()
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
        SceneManager.LoadScene("GameOver");
    }
}


